﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Lab_02.Models;

namespace Lab_02
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private CircleService _circleService;

        private void Update(object sender, RoutedEventArgs e)
        {
            Canvas.Children.Clear();
            _circleService = new CircleService(int.Parse(Input.Text), Canvas);
            _circleService.Run();
            _circleService.Cancel();
        }

        private void Start(object sender, RoutedEventArgs e)
        {
            _circleService.Run();
        }

        private void Cancel(object sender, RoutedEventArgs e)
        {
            _circleService.Cancel();
        }
    }
}
