﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Lab_02.Models
{
    static class CirclesGrid
    {
        private static Random _random = new Random();

        public static Circle[] GetCircles(int count, double width, double height)
        {
            Circle[] outputCircles = new Circle[count];
            GridCell[,] grid = GetGrid(count, width, height);

            for (int i = 0; i < outputCircles.Length;)
            {
                for (int j = 0; j < grid.GetLength(0); j++)
                {
                    for (int k = 0; k < grid.GetLength(1); k++)
                    {
                        if (_random.Next(0, 2) == 1 && grid[j, k].IsEmpty && i < outputCircles.Length)
                        {
                            outputCircles[i] = GetCircle(grid[j, k].Border);
                            grid[j, k].IsEmpty = false;
                            DebugOutput("-----" + "\n" + "Radius: " + outputCircles[i].Radius + "\n" + "Center: " +
                                        outputCircles[i].X + ", " + outputCircles[i].Y + "\n" + "Border: " +
                                        grid[j, k].Border + "\n" + "Cell: " + (j+1) + ", " + (k+1) + "\n");
                            i++;
                        }
                    }
                }
            }

            return outputCircles;
        }

        private static GridCell[,] GetGrid(int count, double width, double height)
        {
            GridCell[,] outputGrid = new GridCell[(int) Math.Ceiling(Math.Sqrt(count)), (int)Math.Ceiling(Math.Sqrt(count))];
            for (int i = 0; i < outputGrid.GetLength(0); i++)
            {
                for (int j = 0; j < outputGrid.GetLength(1); j++)
                {
                    // inside area
                    if (i != 0 && j != 0 && i != outputGrid.GetLength(0) - 1 && j != outputGrid.GetLength(1) - 1)
                        outputGrid[i, j] =
                            new GridCell(new Thickness(outputGrid[i, j - 1].Border.Right,
                                outputGrid[i - 1, j].Border.Bottom, outputGrid[i - 1, j].Border.Right,
                                outputGrid[i, j - 1].Border.Bottom));
                    // left up corner
                    else if (i == 0 && j == 0)
                        outputGrid[i, j] = new GridCell(new Thickness(0, 0, GetMark(width), GetMark(height)));
                    // right up corner
                    else if (i == 0 && j == outputGrid.GetLength(0) - 1)
                        outputGrid[i, j] =
                            new GridCell(new Thickness(outputGrid[i, j - 1].Border.Right, 0, width,
                                outputGrid[i, j - 1].Border.Bottom));
                    // right down corner
                    else if (i == outputGrid.GetLength(0) - 1 && j == outputGrid.GetLength(1) - 1)
                        outputGrid[i, j] =
                            new GridCell(new Thickness(outputGrid[i, j - 1].Border.Right,
                                outputGrid[i, j - 1].Border.Top, width, height));
                    // left down corner
                    else if (j == 0 && i == outputGrid.GetLength(0) - 1)
                        outputGrid[i, j] =
                            new GridCell(new Thickness(0, outputGrid[i - 1, j].Border.Bottom,
                                outputGrid[i - 1, j].Border.Right, height));
                    // up middle area
                    else if (i == 0)
                        outputGrid[i, j] =
                            new GridCell(new Thickness(outputGrid[i, j - 1].Border.Right, 0,
                                GetMark(width, outputGrid[i, j - 1].Border.Right),
                                outputGrid[i, j - 1].Border.Bottom));
                    // right middle area
                    else if (j == outputGrid.GetLength(1) - 1)
                        outputGrid[i, j] =
                            new GridCell(new Thickness(outputGrid[i, j - 1].Border.Right,
                                outputGrid[i, j - 1].Border.Top, width, outputGrid[i, j - 1].Border.Bottom));
                    // down middle area
                    else if (i == outputGrid.GetLength(0) - 1)
                        outputGrid[i, j] =
                            new GridCell(new Thickness(outputGrid[i, j - 1].Border.Right, outputGrid[i, j - 1].Border.Top,
                                outputGrid[i - 1, j].Border.Right, height));
                    // left middle area
                    else if (j == 0)
                        outputGrid[i, j] =
                            new GridCell(new Thickness(0, outputGrid[i - 1, j].Border.Bottom,
                                outputGrid[i - 1, j].Border.Right, GetMark(height, outputGrid[i - 1, j].Border.Bottom)));
                }
            }
            return outputGrid;
        }

        private static double GetMark(double rightSide, double leftSide = 0)
        {
            return _random.Next((int)(leftSide + GetPercentageValue(rightSide - leftSide, 35)),
                (int)(rightSide - GetPercentageValue(rightSide - leftSide, 35)));
        }

        private static double GetPercentageValue(double number, double percent)
        {
            return (number * percent) / 100;
        }

        private static Circle GetCircle(Thickness border)
        {
            double[] center = GetCircleCenter(border);
            double radius = GetCircleRadius(center, border);
            return new Circle(center[0], center[1], radius);
        }

        private static double[] GetCircleCenter(Thickness border)
        {
            double xCenter = border.Left + ((border.Right - border.Left) / 2),
                yCenter = border.Top + ((border.Bottom - border.Top) / 2);
            return new double[]
            {
                _random.Next((int) (xCenter - GetPercentageValue((border.Right - border.Left) / 2, 20)),
                    (int) (xCenter + GetPercentageValue((border.Right - border.Left) / 2, 20))),
                _random.Next((int) (yCenter - GetPercentageValue((border.Bottom - border.Top) / 2, 20)),
                    (int) (yCenter + GetPercentageValue((border.Bottom - border.Top) / 2, 20)))
            };
        }

        private static double GetCircleRadius(double[] circleCenter, Thickness border)
        {
            double radius = circleCenter[0] - border.Left;
            if (circleCenter[1] - border.Top <= radius) radius = circleCenter[1] - border.Top;
            if (border.Right - circleCenter[0] <= radius) radius = border.Right - circleCenter[0];
            if (border.Bottom - circleCenter[1] <= radius) radius = border.Bottom - circleCenter[1];
            return radius - GetPercentageValue(radius, 65);
        }

        private static void DebugOutput(string value)
        {
            Debug.WriteLine(value);
        }
    }

    public class GridCell
    {
        public Thickness Border { get; set; }
        public bool IsEmpty { get; set; }

        public GridCell(Thickness boder)
        {
            Border = boder;
            IsEmpty = true;
        }
    }
}
