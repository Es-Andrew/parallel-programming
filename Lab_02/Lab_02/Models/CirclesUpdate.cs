﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Threading;

namespace Lab_02.Models
{
    class CirclesUpdate
    {
        private DispatcherTimer[] _update;
        private Random _random;
        public ProcessingState State { get; private set; }

        public Circle[] Circles { get; private set; }
        public Canvas Canvas { get; private set; }

        private object _locker = new object();

        public CirclesUpdate(Circle[] circles, Canvas canvas)
        {
            _random = new Random();
            Circles = circles;
            Canvas = canvas;
            InitializeDispatchers();
            State = ProcessingState.Stoped;
        }

        private void InitializeDispatchers()
        {
            _update = new DispatcherTimer[Circles.Length];
            for (int i = 0; i < _update.Length; i++)
            {
                int index = i;
                _update[i] = new DispatcherTimer() {Interval = new TimeSpan(0, 0, 0, 0, 16)};
                _update[i].Tick += (s, a) =>
                {
                    if(State == ProcessingState.Stoped) _update[index].Stop();
                    CalculateCollisions(index);
                };
            }
        }

        public void Run()
        {
            if (State == ProcessingState.Stoped)
                foreach (DispatcherTimer timer in _update)
                    timer.Start();
            State = ProcessingState.Started;
        }

        public void Cancel()
        {
            State = ProcessingState.Stoped;
        }

        private void CalculateCollisions(int circleIndex)
        {
            for (int i = 0; i <= circleIndex; i++)
                lock (_locker)
                {
                    if (Circles[circleIndex] != Circles[i] && IsCollide(Circles[circleIndex], Circles[i]))
                        Circle.CollisionProcessing(Circles[circleIndex], Circles[i]);
                }
            CalculateColisionsWithBorder(Circles[circleIndex]);
            Circles[circleIndex].Move();
        }

        private bool IsCollide(Circle circle, Circle comparableCircle)
        {
            if (
                Math.Sqrt(Math.Pow(circle.X + circle.XSpeed - comparableCircle.X, 2) +
                          Math.Pow(circle.Y + circle.YSpeed - comparableCircle.Y, 2)) <=
                circle.Radius + comparableCircle.Radius)
                return true;
            return false;
        }

        private void CalculateColisionsWithBorder(Circle circle)
        {
            switch (IsCollideWithBorders(circle))
            {
                case Border.Horizontal:
                    circle.XSpeed = -circle.XSpeed;
                    break;
                case Border.Vertical:
                    circle.YSpeed = -circle.YSpeed;
                    break;
            }
        }

        private Border IsCollideWithBorders(Circle cirlce)
        {
            if (cirlce.X - cirlce.Radius + cirlce.XSpeed < 0 || cirlce.X + cirlce.Radius + cirlce.XSpeed > Canvas.ActualWidth)
                return Border.Horizontal;
            if (cirlce.Y - cirlce.Radius + cirlce.YSpeed < 0 || cirlce.Y + cirlce.Radius + cirlce.YSpeed > Canvas.ActualHeight)
                return Border.Vertical;
            return Border.None;
        }
    }
}
