﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Lab_02.Models
{
    class EllipsesUpdate
    {
        private DispatcherTimer[] _update;
        public ProcessingState State { get; private set; }

        public Circle[] Circles { get; private set; }
        public Canvas Canvas { get; private set; }

        public EllipsesUpdate(Circle[] circles, Canvas canvas)
        {
            Circles = circles;
            Canvas = canvas;
            State = ProcessingState.Stoped;
            InitializeDispatchers();
        }

        private void InitializeDispatchers()
        {
            _update = new DispatcherTimer[Circles.Length];
            for (int i = 0; i < _update.Length; i++)
            {
                int index = i;
                _update[i] = new DispatcherTimer() { Interval = new TimeSpan(0, 0, 0, 0, 16) };
                _update[i].Tick += (s, a) =>
                {
                    if(State == ProcessingState.Stoped) _update[index].Stop();
                    UpdateEllipse(index);
                };
            }
        }

        public void Run()
        {
            if(State == ProcessingState.Stoped)
                foreach (DispatcherTimer timer in _update)
                {
                    timer.Start();
                }
            State = ProcessingState.Started;
        }

        public void Cancel()
        {
            State = ProcessingState.Stoped;
        }

        private void UpdateEllipse(int index)
        {
            (Canvas.Children[index] as Ellipse).Margin = new Thickness(Circles[index].X - Circles[index].Radius,
                Circles[index].Y - Circles[index].Radius, 0, 0);
        }
    }
}
