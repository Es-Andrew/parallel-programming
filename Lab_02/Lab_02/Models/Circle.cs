﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.Xaml;

namespace Lab_02.Models
{
    public enum Border
    {
        Horizontal, Vertical, None
    }
    class Circle
    {
        public double X { get; set; }
        public double Y { get; set; }

        public double Radius { get; set; }

        public double XSpeed { get; set; }
        public double YSpeed { get; set; }
        
        public double MaxSpeed { get; private set; }

        private static int _circleCount = 0;
        public int CirlceIndex { get; private set;}

        private static Random _random;

        public Circle(double x, double y, double radius)
        {
            _random = new Random((int)(radius*x*y));
            X = x;
            Y = y;
            Radius = radius;
            XSpeed = GetSpeed();
            YSpeed = GetSpeed();
            MaxSpeed = GetMaxSpeed();
            CirlceIndex = _circleCount++;
        }

        public void Move(double ratio = 1)
        {
                X += XSpeed * ratio;
                Y += YSpeed * ratio;
        }

        public static void CollisionProcessing(Circle circle01, Circle circle02)
        {
            ChangeXSpeed(circle01, circle02);
            ChangeYSpeed(circle01, circle02);
        }

        private static void ChangeXSpeed(Circle circle01, Circle circle02)
        {
            //circle01.XSpeed = -circle01.XSpeed;
            //circle02.XSpeed = -circle02.XSpeed;

            if (IsCoDirected(circle01.XSpeed, circle02.XSpeed))
            {
                if (Math.Abs(circle01.XSpeed) >= Math.Abs(circle02.XSpeed)) circle01.XSpeed = -circle01.XSpeed + GetSpeedAddition(circle01);
                else circle02.XSpeed = -circle02.XSpeed + GetSpeedAddition(circle02);
            }
            else
            {
                double cache = circle01.XSpeed + GetSpeedAddition(circle01);
                circle01.XSpeed = circle02.XSpeed + GetSpeedAddition(circle02);
                circle02.XSpeed = cache;
            }
        }

        private static void ChangeYSpeed(Circle circle01, Circle circle02)
        {
            //circle01.YSpeed = -circle01.YSpeed;
            //circle02.YSpeed = -circle02.YSpeed;

            if (IsCoDirected(circle01.YSpeed, circle02.YSpeed))
            {
                if (Math.Abs(circle01.YSpeed) >= Math.Abs(circle02.YSpeed)) circle01.YSpeed = -circle01.YSpeed + GetSpeedAddition(circle01);
                else circle02.YSpeed = -circle02.YSpeed + GetSpeedAddition(circle02);
            }
            else
            {
                double cache = circle01.YSpeed + GetSpeedAddition(circle01);
                circle01.YSpeed = circle02.YSpeed + GetSpeedAddition(circle02);
                circle02.YSpeed = cache;
            }
        }

        private static bool IsCoDirected(double speed01, double speed02)
        {
            if ((speed01 > 0 && speed02 > 0) || (speed01 < 0 && speed02 < 0)) return true;
            return false;
        }
        private static double GetSpeedAddition(Circle circle)
        {
            if (circle.XSpeed < circle.MaxSpeed && circle.YSpeed < circle.MaxSpeed)
                return _random.Next((int)-circle.MaxSpeed * 15 / 100, (int)circle.MaxSpeed * 15 / 100);
            return 0;
        }

        private double GetMaxSpeed()
        {
            return 6;
        }

        private double GetSpeed()
        {
            double number = 0;
            while (Math.Abs(number) < Double.Epsilon)
                number += _random.Next(-_random.Next(25) * 15 / 100, _random.Next(25) * 15 / 100);
            return number;
        }
    }
}
