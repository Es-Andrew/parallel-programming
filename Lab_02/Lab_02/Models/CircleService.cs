﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Lab_02.Models
{
    public enum ProcessingState { Started, Stoped}
    class CircleService
    {
        public Circle[] Circles { get; set; }
        public Canvas Canvas { get; set; }
        public ProcessingState State { get; private set; }

        private CirclesUpdate _circlesUpdate;
        private EllipsesUpdate _ellipsesUpdate;

        private Random _random;

        public CircleService(int count, Canvas canvas)
        {
            Circles = CirclesGrid.GetCircles(count, canvas.ActualWidth, canvas.ActualHeight);
            Canvas = canvas;
            _random = new Random();
            InitializeCanvasContent(GetEllipses());
            _circlesUpdate = new CirclesUpdate(Circles, Canvas);
            _ellipsesUpdate = new EllipsesUpdate(Circles, Canvas);
            State = ProcessingState.Stoped;
        }

        public void Run()
        {
            _circlesUpdate.Run();
            _ellipsesUpdate.Run();
        }

        public void Cancel()
        {
            _circlesUpdate.Cancel();
            _ellipsesUpdate.Cancel();
        }

        private void InitializeCanvasContent(Ellipse[] ellipses)
        {
            foreach (Ellipse ellipse in ellipses)
                Canvas.Children.Add(ellipse);
        }

        private Ellipse[] GetEllipses()
        {
            Ellipse[] ellipses = new Ellipse[Circles.Length];
            for (int i = 0; i < ellipses.Length; i++)
            {
                ellipses[i] = GetEllipseFromCircle(i);
            }
            return ellipses;
        }

        private Ellipse GetEllipseFromCircle(int index)
        {
            return new Ellipse()
            {
                Fill = new SolidColorBrush(Colors.Aqua),
                Width = 2 * Circles[index].Radius,
                Height = 2 * Circles[index].Radius,
                Stroke = new SolidColorBrush(Colors.Black),
                StrokeThickness = Circles[index].Radius/25,
                Effect = new DropShadowEffect() { Color = Colors.Black,
                    BlurRadius = Circles[index].Radius/15,
                    ShadowDepth = 0,
                    Opacity = 25
                }
            };
        }
    }
}
