﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Lab_02.Services
{
    static class ValidationService
    {
        public static bool IsPalindrome(int inputNumber)
        {
            string number = inputNumber.ToString();
            
            for (int i = 0; i < number.Length / 2; i++)
                if (number[i] != number[number.Length - i - 1]) return false;
            return true;
        }

        public static bool IsSimple(int number)
        {
            for (int i = 2; i < Math.Sqrt(number); i++)
                if (number % i == 0) return false;
            return true;
        }

        public static bool IsNumber(string number)
        {
            return !new Regex("[^0-9]").IsMatch(number);
        }

    }
}
