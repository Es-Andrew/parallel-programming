﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Lab_02.Services
{
    class Consumer
    {
        private Semaphore _semaphoreWorker;
        private Semaphore _semaphoreConsumer;

        private Thread _thread;

        private bool _state;

        private NumbersBuffer _numbersBuffer;

        public Consumer(NumbersBuffer numbersBuffer, Semaphore semaphoreWorker, Semaphore semaphoreConsumer)
        {
            _thread = new Thread(MakeWork);
            _numbersBuffer = numbersBuffer;
            _semaphoreWorker = semaphoreWorker;
            _semaphoreConsumer = semaphoreConsumer;
        }

        public void Run()
        {
            _state = true;
            if (_state && !_thread.IsAlive) _thread.Start();
        }

        private void MakeWork()
        {
            while (_state)
            {
                _semaphoreConsumer.WaitOne();
                int number = _numbersBuffer.Numbers[_numbersBuffer.Head];
                _numbersBuffer.Head = (_numbersBuffer.Head + 1) % _numbersBuffer.Numbers.Length;
                if (ValidationService.IsSimple(number)) SendLog(number);
                _semaphoreWorker.Release();
            }
        }

        private void SendLog(int number)
        {
            LoggingService.AddLog(GenLog(number));
        }

        private Log GenLog(int number)
        {
            return new Log($"{number}");
        }

        public void Stop()
        {
            _state = false;
        }

        public bool IsAlive()
        {
            return _thread.IsAlive;
        }
    }
}