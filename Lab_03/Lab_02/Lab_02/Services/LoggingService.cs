﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Timer = System.Timers.Timer;

namespace Lab_02.Services
{
    class Log
    {
        public TimeSpan Time { get; private set; }
        public String Message { get; private set; }

        private static readonly Stopwatch Stopwatch = new Stopwatch();

        public static void EnableStopWatch()
        {
            if(!Stopwatch.IsRunning) Stopwatch.Start();
        }

        public static void DisableStopWatch()
        {
            if(Stopwatch.IsRunning) Stopwatch.Stop();
        }

        public Log(String message)
        {
            if (isValid(message))
            {
                Message = message;
                Time = !Stopwatch.IsRunning ? DateTime.Now.TimeOfDay : Stopwatch.Elapsed;
            }
            else throw new ArgumentException("Invalid message...");
        }

        private bool isValid(String message)
        {
            return message != null;
        }
    }

    static class LoggingService
    {
        private static List<Log> _logs = new List<Log>();
        private static Timer _timer = new Timer() {Interval = 5000};

        private static string _path = "log.txt";
        private static bool _state = false;

        public static void EnableAutosaving()
        {
            if (!_state)
            {
                _timer.Elapsed += (e, a) =>
                {
                    Save();
                };
                _timer.Start();
                _state = true;
            }
        }

        public static void DisableAutosaving()
        {
            _state = false;
            _timer.Stop();
        }

        private static void Save()
        {
            if (_state)
            {
                File.WriteAllText(_path, getRawLogs());
                Thread.Sleep(2000);
            }
        }

        private static string getRawLogs()
        {
            String rawLog = "";
            foreach (Log log in _logs)
                rawLog += String.Format("\n{1}: {0}", log.Message, log.Time);
            return rawLog;
        }

        public static void AddLog(Log log)
        {
            _logs.Add(log);
            SendToConsole(log);
        }

        private static void SendToConsole(Log log)
        {
            Console.WriteLine("{1}: {0}", log.Message, log.Time);
        }

    }
}
