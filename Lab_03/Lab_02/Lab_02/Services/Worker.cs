﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Lab_02.Services
{
    class Worker
    {
        private Semaphore _semaphoreWorker;
        private Semaphore _semaphoreConsumer;

        private Thread _thread;

        private bool _state;

        private int _from, _to;

        private NumbersBuffer _numbersBuffer;

        public Worker(int from, int to, NumbersBuffer numbersBuffer, Semaphore semaphoreWorker, Semaphore semaphoreConsumer)
        {
            _from = from;
            _to = to;
            _thread = new Thread(MakeWork);
            _numbersBuffer = numbersBuffer;
            _semaphoreWorker = semaphoreWorker;
            _semaphoreConsumer = semaphoreConsumer;
        }

        public void Run()
        {
            _state = true;
            if (_state && !_thread.IsAlive) _thread.Start();
        }

        private void MakeWork()
        {
            for (; _from <= _to && _state; _from++)
                if (ValidationService.IsPalindrome(_from))
                    Send(_from);
        }

        private void Send(int number)
        {
            _semaphoreWorker.WaitOne();
            _numbersBuffer.Numbers[_numbersBuffer.Tail] = number;
            _numbersBuffer.Tail = (_numbersBuffer.Tail + 1) % _numbersBuffer.Numbers.Length;
            _semaphoreConsumer.Release();
        }

        public void Stop()
        {
            _state = false;
        }

        public bool IsAlive()
        {
            return _thread.IsAlive;
        }
    }
}
