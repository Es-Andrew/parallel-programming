﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Timer = System.Timers.Timer;

namespace Lab_02.Services
{
    class ProducersConsumersTask
    {
        private List<Worker> _workers;
        private List<Consumer> _consumers;

        private NumbersBuffer _numbersBuffer;

        private Semaphore _semaphoreWorker;
        private Semaphore _semaphoreConsumer;

        public ProducersConsumersTask(int fromNumber, int toNumber, int bufferLength = 5)
        {
            _numbersBuffer = new NumbersBuffer(bufferLength);
            Initialize(fromNumber, toNumber, bufferLength);
        }

        private bool IsParamatersValid(int bufferLength)
        {
            return bufferLength > 0;
        }

        private void Initialize(int fromNumber, int toNumber, int bufferLenght)
        {
            _semaphoreWorker = new Semaphore(bufferLenght, bufferLenght);
            _semaphoreConsumer = new Semaphore(0, bufferLenght);
            InitializeWorkers(fromNumber, toNumber, 1);
            InitializeConsumers(1);
        }

        private void InitializeWorkers(int fromNumber, int toNumber, int count)
        {
            _workers = new List<Worker>();
            int partValue = (toNumber - fromNumber) / count;
            for (int i = 0; i < count; i++)
                _workers.Add(new Worker(fromNumber + partValue * i,
                    (i + 1) == count ? toNumber : fromNumber + partValue * (i + 1) - 1, _numbersBuffer, _semaphoreWorker, _semaphoreConsumer));
        }

        private void InitializeConsumers(int count)
        {
            _consumers = new List<Consumer>();
            for (int i = 0; i < count; i++)
                _consumers.Add(new Consumer(_numbersBuffer, _semaphoreWorker, _semaphoreConsumer));
        }

        public void Run()
        {
            if (!IsWorkersAlive())
                foreach (Worker worker in _workers)
                    worker.Run();
            if (!IsConsumersAlive())
                foreach (Consumer consumer in _consumers)
                    consumer.Run();
        }

        public void Stop()
        {
            if (IsWorkersAlive())
                foreach (Worker worker in _workers)
                    worker.Stop();
            if (IsConsumersAlive())
                foreach (Consumer consumer in _consumers)
                    consumer.Stop();
        }

        public bool IsWorkersAlive()
        {
            foreach (Worker worker in _workers)
                if (worker.IsAlive()) return true;
            return false;
        }

        public bool IsConsumersAlive()
        {
            foreach (Consumer consumer in _consumers)
                if (consumer.IsAlive()) return true;
            return false;
        }
    }
}
