﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab_02.Services;

namespace Lab_02
{
    class Program
    {
        static void Main(string[] args)
        {
            Log.EnableStopWatch();
            Console.Write("Введите верхнюю границу поиска: ");
            ProducersConsumersTask task = new ProducersConsumersTask(2, GetNumber(), 15);
            task.Run();
        }

        static int GetNumber()
        {
            try
            {
                return int.Parse(Console.ReadLine());
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return 99;
            }
        }
    }
}
