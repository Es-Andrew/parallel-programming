﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab_01.Services;

namespace Lab_02
{
    class Program
    {
        static void Main(string[] args)
        {
            TaskService taskService = new TaskService();
            taskService.Run();
            LoggingService.EnableAutosaving();
        }
    }
}
