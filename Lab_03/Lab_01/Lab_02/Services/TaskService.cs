﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Lab_01.Services
{
    

    class Worker {

        private Thread _thread { get; set; }

        public static int Count = 0;
        public int Index { get; private set; }

        public Resource LeftResource { get; set; }
        public Resource RightResource { get; set; }

        private Random _random;

        public Worker(Resource leftResource, Resource rightResource) {
            LeftResource = leftResource;
            RightResource = rightResource;
            _random = new Random();
            Count++;
            Index = Count;

            _thread = new Thread(Waiting);
            SendLog("инициализирован");
        }

        public void Run()
        {
            if (!_thread.IsAlive) _thread.Start();
        }

        private bool HasAccess() {
            return LeftResource.CheckAccess(this) && RightResource.CheckAccess(this);
        }

        private void Waiting() {
            SendLog("ожидание доступа к ресурсам");
            SendRequest(LeftResource);
            SendRequest(RightResource);
            while (!HasAccess()) Thread.Sleep(GetRandomTime(5,15));
            SendLog("получение доступа к ресурсам");
            Loading();
        }

        private void SendRequest(Resource resource)
        {
            resource.AddToQueue(this);
        }

        private void GetResource(Resource resource) {
            resource.GetResource();
        }

        private void Loading() {
            SendLog("загрузка данных");
            GetResource(LeftResource);
            GetResource(RightResource);
            SendLog("данные загружены");
            Working();
        }

        private void Working() {
            SendLog("обработка данных");
            Thread.Sleep(GetRandomTime(500, 1000));
            SendLog("завершение обработки данных");
            Waiting();
        }

        private void SendLog(String message)
        {
            LoggingService.AddLog(new Log($"{GetName()} {message}"));
        }

        private string GetName()
        {
            return $"[Рабочий-{Index}]";
        }

        private int GetRandomTime(int minValue, int maxValue) {
            return _random.Next(minValue, maxValue);
        }

        
    }

    class Resource {
        public bool State { get; set; }
        private List<int> _advancedQueue;

        private Random _random; 

        public Resource()
        {
            State = true;
            _advancedQueue = new List<int>();
            _random = new Random();
        }

        public void AddToQueue(Worker worker)
        {
            if (_advancedQueue.All(n => n != worker.Index))
                _advancedQueue.Add(worker.Index);
        }

        public bool CheckAccess(Worker worker)
        {
            return _advancedQueue.Count != 0 && _advancedQueue.First() == worker.Index && State;
        }

        public void GetResource()
        {
            State = false;
            Thread.Sleep(GetRandomTime(650, 2000));
            _advancedQueue.RemoveAt(0);
            State = true;
        }

        private int GetRandomTime(int minValue, int maxValue)
        {
            return _random.Next(minValue, maxValue);
        }
    }

    class TaskService {
        private List<Worker> _workers;
        private List<Resource> _resources;

        public TaskService(int workersCount = 5) {
            InitializeResources(workersCount);
            InitializeWorkers(workersCount);
        }

        public void Run()
        {
            foreach (Worker worker in _workers)
            worker.Run();
        }

        private void InitializeResources(int resourcesCount) {
            _resources = new List<Resource>(resourcesCount);
            for (int i = 0; i < resourcesCount; i++) 
                _resources.Add(new Resource());
        }

        private void InitializeWorkers(int workersCount) {
            _workers = new List<Worker>(workersCount);
            for (int i = 0; i < workersCount; i++)
                _workers.Add(GetWorker(i));
        }

        private Worker GetWorker(int index)
        {
            return new Worker(_resources[index], _resources[index + 1 < _resources.Count ? index + 1 : 0]);
        }
    }
}
