﻿using System;
using System.Collections.Generic;
using System.Threading;
using Lab_01.Services;

namespace Lab_03.Services
{
    //TODO: Рабочий загружает ресурсы только при получении доступа сразу к двум...
    //TODO: Использовать Monitor, или locker (логика работы должна выполнятся в одном методе)...
    class Worker {
        enum State { Run, Stop }

        private Thread _thread { get; set; }

        private State _state { get; set; }

        public static int Count = 0;
        public int Index { get; private set; }

        public Resource LeftResource { get; set; }
        public Resource RightResource { get; set; }

        private Random _random;

        public Worker(Resource leftResource, Resource rightResource) {
            LeftResource = leftResource;
            RightResource = rightResource;
            _random = new Random();
            Count++;
            Index = Count;
            _state = State.Stop;

            _thread = new Thread(MakeWork);
            SendLog("инициализирован");
        }

        public void Run()
        {
            _state = State.Run;
            if (!_thread.IsAlive && _state == State.Run) _thread.Start();
        }

        public void Stop()
        {
            _state = State.Stop;
        }

        private void MakeWork()
        {
            while (_state == State.Run)
            {
                GetResources();
                Working();
            }
        }

        //Invalid solution
        private void GetResource(Resource resource) {
            SendLog("ожидание доступа к " + resource.GetName());
            Monitor.Enter(resource.Locker);
            SendLog("доступ к " + resource.GetName() + " получен");
            Thread.Sleep(GetRandomTime(650, 1750));
            Monitor.Exit(resource.Locker);
            SendLog(resource.GetName() + " освобождён");
        }
        
        private void GetResources() {
            SendLog("ожидание доступа к ресурсам");
            Monitor.Enter(LeftResource.Locker);
            Monitor.Enter(RightResource.Locker);
            SendLog("доступ к ресурсам получен");
            Thread.Sleep(GetRandomTime(650,1250));
            Monitor.Exit(RightResource.Locker);
            Monitor.Exit(LeftResource.Locker);
            SendLog("данные загружены");
        }

        private void Working() {
            SendLog("обработка данных");
            Thread.Sleep(GetRandomTime(500, 1000));
            SendLog("завершение обработки данных");
        }

        public void SendLog(String message)
        {
            LoggingService.AddLog(new Log($"{GetName()} {message}"));
        }

        private string GetName()
        {
            return $"[Рабочий-{Index}]";
        }

        private int GetRandomTime(int minValue, int maxValue) {
            return _random.Next(minValue, maxValue);
        }
    }

    class Resource {
        public bool State { get; private set; }
        public object Locker { get; private set; }

        private static int _count = 0;
        public int Index { get; private set; }

        public Resource()
        {
            State = true;
            Locker = new object();
            Index = ++_count;
        }

        public string GetName()
        {
            return $"[Ресурс-{Index}]";
        }
    }

    class TaskService {
        private List<Worker> _workers;
        private List<Resource> _resources;

        public TaskService(int workersCount = 5) {
            InitializeResources(workersCount);
            InitializeWorkers(workersCount);
        }

        public void Run()
        {
            foreach (Worker worker in _workers)
            worker.Run();
        }

        private void InitializeResources(int resourcesCount) {
            _resources = new List<Resource>(resourcesCount);
            for (int i = 0; i < resourcesCount; i++) 
                _resources.Add(new Resource());
        }

        private void InitializeWorkers(int workersCount) {
            _workers = new List<Worker>(workersCount);
            for (int i = 0; i < workersCount; i++)
                _workers.Add(GetWorker(i));
        }

        // Возможны deadlock'и, нужно исправить
        private Worker GetWorker(int index)
        {
            return new Worker(_resources[index], _resources[index + 1 < _resources.Count ? index + 1 : 0]);
        }
    }
}
