﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Lab_01.Services
{
    

    class Worker {
        public enum State {
            Loading,
            Working,
            Waiting
        }

        private Thread _thread { get; set; }
        public State CurrentState { get; set; }

        public static int Count = 0;
        public int Index { get; private set; }

        public Resource LeftResource { get; set; }
        public Resource RightResource { get; set; }

        private Random _random;

        public Worker(Resource leftResource, Resource rightResource) {
            CurrentState = State.Waiting;
            LeftResource = leftResource;
            RightResource = rightResource;
            _random = new Random();
            Count++;
            Index = Count;

            _thread = new Thread(Waiting);
            SendLog(GetName() + " инициализирован");
        }

        public void Run()
        {
            if (!_thread.IsAlive) _thread.Start();
        }

        private bool HasAccess() {
            return LeftResource.State && RightResource.State;
        }

        private void Waiting() {
            CurrentState = State.Waiting;
            SendLog(GetName() + " ожидает доступа к ресурсам");
            GetResource(LeftResource);
            GetResource(RightResource);
            Loading();
        }

        private void GetResource(Resource resource) {
            while (!resource.State) Thread.Sleep(GetRandomTime(5, 15));
            resource.State = false;
        }

        private void SendResource(Resource resource) {
            resource.State = true;
        }

        private void Loading() {
            CurrentState = State.Loading;
            SendLog(GetName() + " загружает данные");
            Thread.Sleep(GetRandomTime(150, 850));
            SendResource(LeftResource);
            SendResource(RightResource);
            Working();
        }

        private void Working() {
            CurrentState = State.Working;
            SendLog(GetName() + " обрабатывает полученные данные");
            Thread.Sleep(GetRandomTime(650, 1750));
            Waiting();
        }

        private void SendLog(String message)
        {
            LoggingService.AddLog(new Log(message));
        }

        private string GetName()
        {
            return $"Рабочий-{Index}";
        }

        private int GetRandomTime(int minValue, int maxValue) {
            return _random.Next(minValue, maxValue);
        }

        
    }

    class Resource {
        public bool State { get; set; }

        public Resource()
        {
            State = true;
        }
    }

    class TaskService {
        private List<Worker> _workers;
        private List<Resource> _resources;

        public TaskService(int workersCount = 5) {
            InitializeResources(workersCount);
            InitializeWorkers(workersCount);
        }

        public void Run()
        {
            foreach (Worker worker in _workers)
            worker.Run();
        }

        private void InitializeResources(int resourcesCount) {
            _resources = new List<Resource>(resourcesCount);
            for (int i = 0; i < resourcesCount; i++) 
                _resources.Add(new Resource());
        }

        private void InitializeWorkers(int workersCount) {
            _workers = new List<Worker>(workersCount);
            for (int i = 0; i < workersCount; i++)
                _workers.Add(GetWorker(i));
        }

        private Worker GetWorker(int index)
        {
            return new Worker(_resources[index], _resources[index + 1 < _resources.Count ? index + 1 : 0]);
        }
    }
}
