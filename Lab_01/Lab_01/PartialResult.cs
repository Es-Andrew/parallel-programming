﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_01
{
    class PartialResult
    {
        private double _value;
        private bool _state;

        public PartialResult()
        {
            _value = 0;
            _state = false;
        }

        public double Value { get { return _value; } set { _value = value; }}
        public bool State { get { return _state; } set { _state = value; } }
    }
}
