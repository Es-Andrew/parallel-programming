﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Timers;
using Timer = System.Timers.Timer;

namespace Lab_01
{
    class DefIntegral
    {
        private double[] _segments;
        private PartialResult[] _partialResults;
        private int _subdivides;
        private bool _isCompleted;
        private Func<double, double> _function;

        public DefIntegral(Func<double, double> function,int segments, double from, double to, int subdivides)
        {
            _partialResults = new PartialResult[segments];
            for (int i = 0; i < segments; i++)
                _partialResults[i] = new PartialResult();
            _isCompleted = false;
            _function = function;
            _subdivides = subdivides;
            _segments = new double[segments+1];
            _segments[0] = from;
            _segments[segments] = to;
            double inc = (to - from) / segments;
            for (int i = 1; i < segments; i++)
                _segments[i] = _segments[i - 1] + inc;
        }

        public void Calculate(ThreadPriority threadPriority)
        {
            double subdivisionLenght = (_segments[1] - _segments[0]) / _subdivides;
            for (int i = 0; i < _segments.Length - 1; i++)
            {
                int indexPoint = i;
                Thread thread = new Thread(delegate() { GetAreaRectangle(indexPoint, subdivisionLenght); })
                {
                    Priority = threadPriority
                };
                thread.Start();
            }
            Thread eventThread = new Thread(OperationsCompleted);
            eventThread.Start();
        }

        private void GetAreaRectangle(int indexPoint, double subdivisionLeght)
        {
            for (double j = _segments[indexPoint]; j < _segments[indexPoint + 1]; j += subdivisionLeght)
                {
                    _partialResults[indexPoint].Value += _function(j) * subdivisionLeght;
                }
            _partialResults[indexPoint].State = true;
        }

        private void OperationsCompleted()
        {
            Timer timer = new Timer() {Interval = 100};
            timer.Elapsed += (s, e) =>
                {
                    if (_isCompleted == false)
                    {
                        _isCompleted = true;
                        foreach (PartialResult partialResult in _partialResults)
                            if (partialResult.State == false)
                                _isCompleted = false;
                    }
                    else
                    {
                        ConsolePrint();
                        timer.Stop();
                    }
                }
            ;
            timer.Start();
        }

        private void ConsolePrint()
        {
            Console.WriteLine(Result);
        }

        public double Result
        {
            get
            {
                double result = 0;
                foreach (PartialResult partialResult in _partialResults)
                {
                    result += partialResult.Value;
                }
                return result;
            }
        }

    }
}
