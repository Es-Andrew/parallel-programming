﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Lab_01
{
    class Program
    {
        static void Main(string[] args)
        {
            DefIntegral dIntegral = new DefIntegral(
                functionArgument => Math.Sin(functionArgument) * Math.Cos(Math.Pow(functionArgument, 2))
                ,3, 0, 10, 5000000);
            dIntegral.Calculate(ThreadPriority.Normal);
            Console.WriteLine("Threads working!");
            Console.Read();
        }

        static void Print(string content)
        {
            Console.WriteLine(content);
        }
    }
}
