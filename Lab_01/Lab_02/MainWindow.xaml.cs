﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using ThreadState = System.Threading.ThreadState;

namespace Lab_02
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            _quickSortStep = 0;
            _exchangeSortStep = 0;
            InitializeComponent();
        }

        private Thread _thread01;
        private Thread _thread02;
        private int[] _inputArrayQuickSort;
        private int[] _inputArrayExchangeSort;

        private double _quickSortStep;
        private double _exchangeSortStep;


        private void Start_OnClick(object sender, RoutedEventArgs e)
        {
            SetActionButtonsActive();
            ButtonStart.IsEnabled = false;

            _quickSortStep = 0;
            _exchangeSortStep = 0;

            ProgressBar01.Maximum = int.Parse(CountArraysObjects.Text);
            ProgressBar02.Maximum = int.Parse(CountArraysObjects.Text);

            _inputArrayQuickSort = new int[int.Parse(CountArraysObjects.Text)];
            _inputArrayExchangeSort = new int[int.Parse(CountArraysObjects.Text)];

            InitArrays(0, int.Parse(CountArraysObjects.Text));

            _thread01 = new Thread(delegate()
            {
                SortQuick(0,_inputArrayQuickSort.Length - 1);
            });

            _thread02 = new Thread(SortExchange);

            _thread02.Start();
            _thread01.Start();
            UpdatingUi();
        }

        private void SetActionButtonsActive()
        {
            ButtonAbort01.IsEnabled = true;
            ButtonAbort02.IsEnabled = true;
            ButtonStatusChange01.IsEnabled = true;
            ButtonStatusChange02.IsEnabled = true;
        }

        private void SetActionButtonsDisable()
        {
            ButtonAbort01.IsEnabled = false;
            ButtonAbort02.IsEnabled = false;
            ButtonStatusChange01.IsEnabled = false;
            ButtonStatusChange02.IsEnabled = false;
        }

        private void InitArrays(int minValue, int maxValue)
        {
            Random random = new Random();
            for (int i = 0; i < _inputArrayQuickSort.Length; i++)
            {
                _inputArrayQuickSort[i] = random.Next(minValue, maxValue);
                _inputArrayExchangeSort[i] = _inputArrayQuickSort[i];
            }
        }

        private void SortQuick(int begin, int end)
        {
            int i = begin, j = end;
            int baseNumber = _inputArrayQuickSort[(begin + end) / 2];
            while (i <= j)
            {
                while (_inputArrayQuickSort[i] < baseNumber)
                    i++;
                while (_inputArrayQuickSort[j] > baseNumber)
                    j--;
                if (i <= j)
                {
                    int cache = _inputArrayQuickSort[i];
                    _inputArrayQuickSort[i] = _inputArrayQuickSort[j];
                    _inputArrayQuickSort[j] = cache;
                    i++;
                    j--;
                }
            }
            
            if (begin < j) SortQuick(begin, j);
            if (i < end) SortQuick(i, end);
            _quickSortStep = end + 1;
        }

        private void SortExchange()
        {
            for (int i = 0; i < _inputArrayExchangeSort.Length; i++)
            {
                for (int j = 0; j < _inputArrayExchangeSort.Length - i - 1; j++)
                {
                    if (_inputArrayExchangeSort[j] > _inputArrayExchangeSort[j + 1])
                    {
                        int cache = _inputArrayExchangeSort[j];
                        _inputArrayExchangeSort[j] = _inputArrayExchangeSort[j + 1];
                        _inputArrayExchangeSort[j + 1] = cache;
                    }
                }
                _exchangeSortStep += 1;
            }
        }

        private void UpdatingUi()
        {
            DispatcherTimer timer = new DispatcherTimer {Interval = new TimeSpan(0, 0, 0, 0, 20)};
            timer.Tick += ((sender, e) =>
            {
                ProgressBar01.Value = _quickSortStep;
                ProgressBar02.Value = _exchangeSortStep;
                TextBlock.Text = _quickSortStep.ToString();
                if (!_thread01.IsAlive && !_thread02.IsAlive)
                {
                    ButtonStart.IsEnabled = true;
                    SetActionButtonsDisable();
                    ButtonStatusChange01.Content = "\xe103";
                    ButtonStatusChange02.Content = "\xe103";
                    timer.Stop();
                }
            });
            timer.Start();
        }

        private void Save_OnClick(object sender, RoutedEventArgs e)
        {
            
        }

        private void ButtonStatusChange01_OnClick(object sender, RoutedEventArgs e)
        {
            switch (_thread01.ThreadState)
            {
                case ThreadState.Running:
                {
                    _thread01.Suspend();
                    ButtonStatusChange01.Content = "\xe102";
                }
                    break;
                case ThreadState.Suspended:
                {
                    _thread01.Resume();
                    ButtonStatusChange02.Content = "\xe103";
                    }
                    break;
            }
        }

        private void ButtonAbort01_OnClick(object sender, RoutedEventArgs e)
        {
            if (_thread01 != null && _thread01.IsAlive) _thread01.Abort();
            ButtonAbort01.IsEnabled = false;
        }

        private void ButtonStatusChange02_OnClick(object sender, RoutedEventArgs e)
        {
            switch (_thread02.ThreadState)
            {
                case ThreadState.Running:
                {
                    _thread02.Suspend();
                    ButtonStatusChange02.Content = "\xe102";
                }
                    break;
                case ThreadState.Suspended:
                {
                    _thread02.Resume();
                    ButtonStatusChange02.Content = "\xe103";
                }
                    break;
            }
        }

    private void ButtonAbort02_OnClick(object sender, RoutedEventArgs e)
        {
            if(_thread02 != null && _thread02.IsAlive) _thread02.Abort();
            ButtonAbort02.IsEnabled = false;
        }

        private void PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = IsTextAllowed(e.Text);
        }

        private bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[0-9]+");
            return !regex.IsMatch(text);
        }
}
}
